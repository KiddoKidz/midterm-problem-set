public class Knife extends Weapon {

    public Knife(String name, int attackPoint, int defensePoint, int durabilityPoint) {
        super(name, attackPoint, defensePoint, durabilityPoint);
    }

    @Override
    public void chantHealingPoem(Character aCharacter){
        System.out.println("Unfortunately, "+this.getName()+" cannot be used to cast a healing spell");
    }

    @Override
    public void block(Character aCharacter){
        System.out.println("Unfortunately, "+this.getName()+" cannot be used to block attack");
    }
}
